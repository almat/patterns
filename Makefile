#---------------
# [ ENV ]
#---------------
-include .env.php

rmgit: # Удаляет git
	rm -v ./www/.gitkeep
	rm -rfv .git

ps: ## Список запущенных контейнеров.
	docker-compose ps

build: ## Сборка образов
	docker compose -f docker-compose.yml -f docker-compose.override.yml build

up: ## Создаем(если нет) образы и контейнеры, запускаем контейнеры.
	docker compose -f docker-compose.yml -f docker-compose.override.yml up -d

stop: ## Останавливает контейнеры.
	docker compose -f docker-compose.yml -f docker-compose.override.yml stop

down: ##Останавливает, удаляет контейнеры. docker-compose down --remove-orphans
	docker compose -f docker-compose.yml -f docker-compose.override.yml down --remove-orphans

down-clear: ##Останавливает, удаляет контейнеры и volumes. docker-compose down -v --remove-orphans
	docker compose -f docker-compose.yml -f docker-compose.override.yml down -v --remove-orphans

console-php: ##php консоль под www-data
	docker exec -it --user www-data ${COMPOSE_PROJECT_NAME}-php-1 sh

console-php-root: ##php консоль под root
	docker exec -it --user root ${COMPOSE_PROJECT_NAME}-php-1 sh

run-test: ##php тесты
	docker exec -it --user root ${COMPOSE_PROJECT_NAME}-php-1 ./vendor/bin/phpunit --bootstrap tests/autoload.php tests