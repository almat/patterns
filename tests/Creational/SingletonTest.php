<?php

namespace Creational;

use Patterns\Creational\Singleton\Singleton;
use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function testIsUniq()
    {
        $firstCall = Singleton::getInstance();
        $secondCall = Singleton::getInstance();

        $this->assertInstanceOf(Singleton::class, $firstCall);
        $this->assertSame($firstCall, $secondCall);

        $this->expectException('Error');
        $new = new Singleton();
    }

    public function testClone()
    {
        $instance = Singleton::getInstance();
        $this->expectException('Error');
        $new = clone $instance;
    }

    public function testSerialize()
    {
        $instance = Singleton::getInstance();
        $serialized = serialize($instance);

        $this->expectException('Exception');
        $new = unserialize($serialized);
    }
}