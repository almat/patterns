<?php

declare(strict_types=1);

namespace Patterns\Creational\Singleton;

use Exception;

final class Singleton
{
    private static ?Singleton $instance = null;

    public static function getInstance(): Singleton
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Can't create instance
     */
    private function __construct()
    {
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function __clone()
    {
        throw new Exception("Cannot clone singleton");
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }
}